package ua.dashboard.prototype

import com.github.appreciated.card.Card
import com.vaadin.flow.component.html.Div
import com.vaadin.flow.component.html.H1
import com.vaadin.flow.component.orderedlayout.FlexComponent
import com.vaadin.flow.component.orderedlayout.HorizontalLayout
import com.vaadin.flow.component.orderedlayout.VerticalLayout
import com.vaadin.flow.router.Route
import com.vaadin.flow.server.PWA

@Route(value = "", layout = MyAppLayoutRouterLayout::class)
@PWA(name = "Dashboard prototype", shortName = "prototype")
class MainView : VerticalLayout {

    constructor() {
        setWidthFull()
        alignItems = FlexComponent.Alignment.CENTER

        val hl1 = HorizontalLayout()
        val hl2 = HorizontalLayout()

        val header = H1("HELLO THERE")

        val chart1 = AreaChartExample(listOf(150.0, 41.0, 35.0, 31.0, 49.0, 22.0, 69.0, 21.0, 14.0))
        val card1 = Div(chart1)
        card1.width = "400px"


        val chart4 = AreaChartExample(listOf(10.0, 41.0, 35.0, 51.0, 49.0, 62.0, 69.0, 91.0, 148.0))
        val card4 = Div(chart4)
        card4.width = "400px"

        val chart5 = AreaChartExample(listOf(10.0, 141.0, 35.0, 21.0, 49.0, 62.0, 69.0, 31.0, 78.0))
        val card5 = Div(chart5)
        card5.width = "400px"

        val chart2 = GradientRadialBarChartExample()
        val card2 = Div(chart2)
        card2.width = "600px"

        val chart3 = RadarChartExample()
        val card3 = Div(chart3)
        card3.width = "600px"

        alignItems = FlexComponent.Alignment.CENTER
        isMargin = true

        hl1.add(card1, card4, card5)
        hl2.add(card2, card3)
        add(hl1, hl2)
    }
}
