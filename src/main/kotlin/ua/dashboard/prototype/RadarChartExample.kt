package ua.dashboard.prototype

import com.github.appreciated.apexcharts.ApexCharts
import com.github.appreciated.apexcharts.config.builder.ChartBuilder
import com.github.appreciated.apexcharts.config.builder.TitleSubtitleBuilder
import com.github.appreciated.apexcharts.config.chart.Type
import com.github.appreciated.apexcharts.helper.Series
import com.github.appreciated.card.Card
import com.vaadin.flow.component.html.Div

class RadarChartExample : Card {
    constructor() {
        val radarChart = ApexCharts()
                .withChart(ChartBuilder.get()
                        .withType(Type.radar)
                        .build())
                .withSeries(Series("Series 1", 80, 50, 30, 40, 100, 20))
                .withTitle(TitleSubtitleBuilder.get()
                        .withText("Basic Radar Chart")
                        .build())
                .withLabels("January", "February", "March", "April", "May", "June")
        width="100%"
        maxWidth = "350px"
        add(radarChart)
    }
}
