package ua.dashboard.prototype

import com.github.appreciated.apexcharts.ApexCharts
import com.github.appreciated.apexcharts.config.Responsive
import com.github.appreciated.apexcharts.config.builder.*
import com.github.appreciated.apexcharts.config.chart.Type
import com.github.appreciated.apexcharts.config.chart.builder.ZoomBuilder
import com.github.appreciated.apexcharts.config.legend.HorizontalAlign
import com.github.appreciated.apexcharts.config.stroke.Curve
import com.github.appreciated.apexcharts.config.subtitle.Align
import com.github.appreciated.apexcharts.config.xaxis.XAxisType
import com.github.appreciated.apexcharts.helper.Series
import com.github.appreciated.card.Card
import com.vaadin.flow.component.html.Div
import com.vaadin.flow.component.orderedlayout.FlexComponent

import java.time.LocalDate
import java.util.stream.Collector
import java.util.stream.IntStream
import kotlin.streams.toList

internal class AreaChartExample : Card {
    constructor(data: List<Double>) {
        val areaChart = ApexCharts()
                .withChart(
                        ChartBuilder.get()
                                .withType(Type.area)
                                .withZoom(ZoomBuilder.get()
                                        .withEnabled(false)
                                        .build())
                                .build())
                .withDataLabels(DataLabelsBuilder.get()
                        .withEnabled(false)
                        .build())
                .withStroke(StrokeBuilder.get().withCurve(Curve.smooth).build())
                .withSeries(Series("STOCK ABC", *data.toTypedArray()))
//                .withTitle(TitleSubtitleBuilder.get()
//                        .withText("Fundamental Analysis of Stocks")
//                        .withAlign(Align.left).build())
//                .withSubtitle(TitleSubtitleBuilder.get()
//                        .withText("Price Movements")
//                        .withAlign(Align.left).build())
               // .withLabels(*IntStream.range(1, 10).boxed().map { day -> LocalDate.of(2000, 1, day!!).toString() }.toList().toTypedArray())
//                .withXaxis(XAxisBuilder.get()
//                        .withType(XAxisType.datetime).build())
                .withColors("#ff8161")
                .withYaxis(YAxisBuilder.get().withShow(false)
                        .withOpposite(true).build())
                .withColors("#ff8181")
                .withLegend(LegendBuilder.get().withHorizontalAlign(HorizontalAlign.left).build())

        width="100%"
maxWidth = "350px"
        add(areaChart)

    }
}
