package ua.dashboard.prototype

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
class PrototypeApplication

fun main(args: Array<String>) {
    SpringApplication.run(PrototypeApplication::class.java, *args)
}



