package ua.dashboard.prototype

import com.github.appreciated.app.layout.behaviour.Behaviour
import com.github.appreciated.app.layout.builder.AppLayoutBuilder
import com.github.appreciated.app.layout.component.appbar.AppBarBuilder
import com.github.appreciated.app.layout.component.menu.left.builder.LeftAppMenuBuilder
import com.github.appreciated.app.layout.component.menu.left.builder.LeftSubMenuBuilder
import com.github.appreciated.app.layout.component.menu.left.items.LeftClickableItem
import com.github.appreciated.app.layout.component.menu.left.items.LeftHeaderItem
import com.github.appreciated.app.layout.component.menu.left.items.LeftNavigationItem
import com.github.appreciated.app.layout.design.Styles
import com.github.appreciated.app.layout.entity.DefaultBadgeHolder
import com.github.appreciated.app.layout.entity.Section
import com.github.appreciated.app.layout.notification.DefaultNotificationHolder
import com.github.appreciated.app.layout.notification.component.AppBarNotificationButton
import com.github.appreciated.app.layout.notification.entitiy.DefaultNotification
import com.github.appreciated.app.layout.router.AppLayoutRouterLayout
import com.vaadin.flow.component.dependency.HtmlImport
import com.vaadin.flow.component.dependency.StyleSheet
import com.vaadin.flow.component.html.Image
import com.vaadin.flow.component.icon.VaadinIcon
import com.vaadin.flow.component.notification.Notification
import com.vaadin.flow.component.page.Push
import com.vaadin.flow.dom.Style

@Push
@HtmlImport("frontend://src/custom-style.html")
@StyleSheet("frontend://styles.css")
class MyAppLayoutRouterLayout : AppLayoutRouterLayout {

    private val notifications: DefaultNotificationHolder
    private val badge: DefaultBadgeHolder

    constructor() {
        notifications = DefaultNotificationHolder({ newStatus -> })
        badge = DefaultBadgeHolder(5)
        val menuEntry = LeftNavigationItem("Menu", VaadinIcon.MENU.create(), MainView::class.java)

        val home = LeftNavigationItem("Home", VaadinIcon.HOME.create(), MainView::class.java)
        home.style.set("color", "red")
        home.style.set("background", "#ffffff")

        val logo = Image("https://www.homecredit.vn/img/logo-hc2016-main-red.png","")
        logo.width = "100px"

        val notificationButton = AppBarNotificationButton(VaadinIcon.BELL, notifications)
        notificationButton.setClassName("notification-icon")

        badge.bind(menuEntry.getBadge())
        init(AppLayoutBuilder
                .get(Behaviour.LEFT_RESPONSIVE_HYBRID)
                .withTitle(logo)
                .withAppBar(AppBarBuilder.get()
                        .add(AppBarNotificationButton(VaadinIcon.BELL, notifications))
                        .build())
                .withAppMenu(LeftAppMenuBuilder.get()
                        .addToSection(LeftClickableItem("Clickable Entry", VaadinIcon.COG.create()) { clickEvent -> Notification.show("onClick ...") }, Section.HEADER)
                        .add(home)
                        .add(LeftSubMenuBuilder.get("My Submenu", VaadinIcon.PLUS.create())
                                .add(LeftSubMenuBuilder.get("My Submenu", VaadinIcon.PLUS.create())
                                        .add(LeftNavigationItem("Charts", VaadinIcon.SPLINE_CHART.create(), MainView::class.java))
                                        .add(LeftNavigationItem("Contact", VaadinIcon.CONNECT.create(), MainView::class.java))
                                        .add(LeftNavigationItem("More", VaadinIcon.COG.create(), MainView::class.java))
                                        .build())
                                .add(LeftNavigationItem("Contact1", VaadinIcon.CONNECT.create(), MainView::class.java))
                                .add(LeftNavigationItem("More1", VaadinIcon.COG.create(), MainView::class.java))
                                .build())
                        .add(menuEntry)
                        .addToSection(LeftClickableItem("Clickable Entry", VaadinIcon.COG.create()) { clickEvent -> Notification.show("onClick ...") }, Section.FOOTER)
                        .build())
                .build())
    }
}
